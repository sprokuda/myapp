import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.2
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.2

Window {

//  singnal emitted by encBottom click, varialble text in passed to c++ slot fuction
    signal submitEncTextField(string text)

//  singnal emitted by encBottom click
    signal submitDecTextField()

// the slot to get encrypted data from c++ code
    function setEncField(text){
//        console.log("encryptedTextFiled: " + text)
        encryptedTextField.text = text
    }
// the slot to get decrypted data from c++ code
    function setDecField(text){
//        console.log("decryptedTextField: " + text)
        decryptedTextField.text = text
    }
// function to be invoked to set publicKeyTextFieldat start up
    function setPublicKeysField(text){
//        console.log("publicKeyTextField: " + text)
        publicKeyTextField.text = text

    }
// function to be invoked to set setSecretKeysField start up
    function setSecretKeysField(text){
//        console.log("publicKeyTextField: " + text)
        secretKeyTextField.text = text

    }
//parameters of main window
    visible: true
    width: 950
    height: 300
    color: "#ccd9d9"

//text field to enter some text to be encrypted
    TextField {
        id: inputTextField
        x: 580
        y: 140
        width: 235
        height: 25
        placeholderText: qsTr("Enter some text...")
    }
//button to do encryption of inputTextField by calling c++ fuction by sending submitEncTextField signal
    Button {
        id: encButton
        x: 835
        y: 140
        width: 85
        height: 25
        text: qsTr("Encrypt")

        onClicked:
            // emit the submitTextField signal
            submitEncTextField(inputTextField.text)
    }
//public key
    TextField {
        id: publicKeyTextField
        objectName: "publicKeyTextField"
        x: 30
        y: 30
        width: 890
        height: 25
        readOnly: true
        placeholderText: qsTr("")
    }
//secret key
    TextField {
        id: secretKeyTextField
        x: 30
        y: 85
        width: 890
        height: 25
        readOnly: true
        placeholderText: qsTr("")
    }
// caption for public key text field
    Text {
        id: publicKeyCaption
        x: 30
        y: 10
        width: 90
        height: 15
        text: qsTr("Public Key")
        font.pixelSize: 12
    }
// caption for secret key text field
    Text {
        id: secretKeyCaption
        x: 30
        y: 65
        width: 90
        height: 15
        text: qsTr("Secret Key")
        font.pixelSize: 12
    }
// caption for input text field
    Text {
        id: inputTextCaption
        x: 580
        y: 120
        width: 120
        height: 15
        text: qsTr("Enter text to encrypt")
        font.pixelSize: 12
    }
// text field for encrypted text field
    TextField {
        id: encryptedTextField
        x: 30
        y: 195
        width: 890
        height: 25
        readOnly: true
        placeholderText: qsTr("")
    }
// caption for encrypted text field
    Text {
        id: encryptedTextCaption
        x: 30
        y: 175
        width: 90
        height: 15
        text: qsTr("Encrypted text")
        font.pixelSize: 12
    }
// text field for decrypted text field
    TextField {
        id: decryptedTextField
        x: 580
        y: 250
        width: 235
        height: 25
        readOnly: true
        placeholderText: qsTr("")
    }
//button to do decryption of encrypted text by calling c++ fuction by sending submitDecTextField signal
    Button {
        id: decButton
        x: 835
        y: 250
        width: 85
        height: 25
        text: qsTr("Decrypt")
        onClicked:
            // emit the submitTextField signal
            submitDecTextField()
    }
// caption for decrypted text field
    Text {
        id: decryptedTextCaption
        x: 580
        y: 230
        width: 120
        height: 15
        text: qsTr("Decrypted text")
        font.pixelSize: 12
    }
}




/*import QtQuick 2.9
import QtQuick.Controls 2.2
//import QtQuick.Layouts 1.0
//import QtQuick.Dialogs 1.0


import QtQuick.Window 2.2


Window {
    signal submitTextField(string text)

    visible: true
    width: 640
    height: 480
    title: qsTr("Scroll")

//Item{
//    id: item

//    signal qmlSignal(msg: string)
////     width: 400; height: 400

            Button {
                id: button
                x: 450
                y: 100
                text: qsTr("Button")
                anchors.fill: parent

                onClicked: submitTextField("Hello from QML")
            }


        TextField {
            id: textField_1
            objectName: "textField_1"
            x: 100
            y: 100
            width: 300
            height: 40
            text: qsTr("Text Field")
        }
//}

}
*/
