#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <memory>
#include "handle.h"

using namespace std;

QString trueHexView(unsigned char bytes[], size_t size)
{
    //make poiter to store output, memory leackage safe
    unique_ptr<char[]> myHex = make_unique<char[]>((size * 5) + 1);

    for (size_t i = 0; i < size; i++)
    {
        //print bytes in hex form to output pointer
        sprintf(myHex.get() + (i * 5), " 0x%02X", bytes[i]);
    }
    //creating QString oject from raw pointer
    QString result(myHex.get());
    return result;
}


void HandleTextField::Init()
{
    //sef of decryption enabling flag
    m_ifEnc= false;
    //calculation of values of public and secret keys
    crypto_box_keypair(m_publicKey,m_secretKey);
}

QVariant HandleTextField::getPublicKey()
{
    //return of hex view of public key wrapped in QVariant for qml reciever
    return QVariant( trueHexView(m_publicKey, crypto_box_PUBLICKEYBYTES ) );
}

QVariant HandleTextField::getSecretKey()
{
    //return of hex view of secret key wrapped in QVariant for qml reciever
    return QVariant( trueHexView(m_secretKey, crypto_box_SECRETKEYBYTES ) );
}

void HandleTextField::handleInpTextField(const QString& in)
{
    //initialisation of input text message member
    m_msgSize = in.size();
    m_inpMsg = std::make_unique<unsigned char[]>(m_msgSize);
    memcpy(m_inpMsg.get(), in.toStdString().c_str(),m_msgSize);

    //calculation of nonce data for encryption/decryption
    randombytes(m_Nonce, sizeof(m_Nonce));

    //creation of pointer for padded data
    unique_ptr<unsigned char[]> padded = std::make_unique<unsigned char[]>(crypto_box_ZEROBYTES + m_msgSize);

    //completion of padded array with data
    memset(padded.get(), 0, crypto_box_ZEROBYTES);
    memcpy(padded.get() + crypto_box_ZEROBYTES, m_inpMsg.get(), m_msgSize);

    // calculation of encoded message size
    m_encSize = crypto_box_ZEROBYTES + m_msgSize;

    //pointer to store encrypted message
    m_Enc = std::make_unique<unsigned char[]>(m_encSize);

    // Encryption of input message
    crypto_box(m_Enc.get(), padded.get(), m_encSize, m_Nonce, m_publicKey, m_secretKey);

    //signal for qml fucntion to set encrypted text field with data being sent
    emit setEncField(trueHexView(m_Enc.get() + crypto_box_BOXZEROBYTES,m_encSize - crypto_box_BOXZEROBYTES));

    //flag to enable decoding
    m_ifEnc = true;
}

  void HandleTextField::handleOutTextField()
{
    //chech if encryption is done
    if(m_ifEnc)
    {
    //creation of pointer for outpus message data
    unique_ptr<unsigned char[]> message = std::make_unique<unsigned char[]>(m_msgSize + crypto_box_ZEROBYTES);

    // Decrypt encrypted message
    crypto_box_open(message.get(), m_Enc.get(), m_msgSize + crypto_box_ZEROBYTES, m_Nonce, m_publicKey, m_secretKey);

    //string manipulations to erase first zeros
    string s( reinterpret_cast<char const*>(message.get()),m_msgSize + crypto_box_ZEROBYTES ) ;
    s.erase(0,crypto_box_ZEROBYTES);

    //wrapping of output string in QString to be received for qml function
    QString outMsg = QString(s.c_str());

    //send singnal to qml
    emit setDecField(outMsg);
    }
}
