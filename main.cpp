#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>

#include <handle.h>


int main(int argc, char *argv[])
{
    //gui application object
    QGuiApplication app(argc, argv);

    //object to handle qml code
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    //getting access to qml window created
    QObject *topLevel = engine.rootObjects().value(0);
    QQuickWindow *window = qobject_cast<QQuickWindow *>(topLevel);

    HandleTextField handleTextField;
    //dumny vaiable for invokeMethod
    QVariant returnedValue;

    //call of QML functions to set text field at window start up
    QMetaObject::invokeMethod(window, "setPublicKeysField",
            Q_RETURN_ARG(QVariant,returnedValue),
            Q_ARG(QVariant, handleTextField.getPublicKey()));

    QMetaObject::invokeMethod(window, "setSecretKeysField",
            Q_RETURN_ARG(QVariant,returnedValue),
            Q_ARG(QVariant, handleTextField.getSecretKey()));

    // connect our QML signals to C++ slots
    QObject::connect(window, SIGNAL(submitEncTextField(QString)),
                         &handleTextField, SLOT(handleInpTextField(QString)));

    QObject::connect(window, SIGNAL(submitDecTextField()),
                         &handleTextField, SLOT(handleOutTextField()));

    // connect our C++ signals to our QML slots
    // NOTE: if we want to pass an parameter to our QML slot, it has to be
    // a QVariant.
    QObject::connect(&handleTextField, SIGNAL(setEncField(QVariant)),
                         window, SLOT(setEncField(QVariant)));

    QObject::connect(&handleTextField, SIGNAL(setDecField(QVariant)),
                         window, SLOT(setDecField(QVariant)));
    //start of main loop execution
    return app.exec();
}

