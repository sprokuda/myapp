#ifndef CODE1_H
#define CODE1_H
#include <QObject>
#include <QDebug>

#include <memory>

#include "tweetnacl.h"
#include "randombytes.h"
#include "tools.h"

using namespace std;
// function ot get string representation of hex numbers
QString trueHexView(unsigned char bytes[], size_t size);

class HandleTextField : public QObject
{
    Q_OBJECT

public:
    explicit HandleTextField(QObject *parent = 0) :
    QObject(parent){ Init(); }

    //initialization of static allocated members needed at window start
    void Init();
    //return public key of encryption routine
    QVariant getPublicKey();
    //return public key of encryption routine
    QVariant getSecretKey();

public slots:
    //gets and handles input test field data, encrypting of input text
    void handleInpTextField(const QString& in);
    //handles and sets output text field,
    //decrypting of encryption done in handleInpTextField
    void handleOutTextField();

signals:
    // signal with encrypted text as argument, received by qml fuction
    void setEncField(QVariant text);
    // signal with decrypted text as argument, received by qml fuction
    void setDecField(QVariant text);

private:
    //nonce data
    unsigned char m_Nonce[crypto_box_NONCEBYTES];
    //flag to enable decryption
    bool m_ifEnc;
    //size of encrypted text (bytes)
    size_t m_msgSize;
    //size of output message text with zeros (bytes)
    size_t m_encSize;
    //pointer to handle input message
    unique_ptr<unsigned char[]> m_inpMsg;
    //pointer to handle encrypted input message
    unique_ptr<unsigned char[]> m_Enc;
    //public key of encryption
    unsigned char m_publicKey[crypto_box_PUBLICKEYBYTES];
    //secret key of encryption
    unsigned char m_secretKey[crypto_box_SECRETKEYBYTES];
};


#endif // CODE1_H
